<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="proteus-sn-export" Label="Export to Proteus-SN" BaseType="operation" Version="0">
      <ItemDefinitions>
        <Component Name="model" Label="Model">
          <Accepts>
            <Resource Name="smtk::session::rgg::Resource" Filter="model"/>
          </Accepts>
        </Component>
        <Resource Name="attributes" Label="Attributes">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <Directory Name="OutputDirectory" Label="Output Directory" Version="0" NumberOfRequiredValues="1">
          <BriefDescription>The directory to write</BriefDescription>
        </Directory>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
