<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">

  <Definitions>

    <AttDef Type="mcc3" BaseType="" Unique="true" Associations="">

      <ItemDefinitions>

        <Void Name="force_mixture_calc" Label="Force Mixture Calculation" Optional="true" IsEnabledByDefault="false"/>

	<String Name="xslib" Label="Cross Section Library" AdvanceLevel="0" NumberOfRequiredValues="1">
          <BriefDescription>The cross section library used by mcc3</BriefDescription>
          <DetailedDescription>endf7.0 and endf7.1 are the only
          cross section library currently available</DetailedDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="endf7.0">endf7.0</Value>
            <Value Enum="endf7.1">endf7.1</Value>
          </DiscreteInfo>
	</String>

        <String Name="Energy Group">

          <ChildrenDefinitions>

	    <String Name="egroupname" Label="Energy Group Structure"
                    AdvanceLevel="0" NumberOfRequiredValues="1">
              <BriefDescription>Predefined energy group</BriefDescription>
              <DetailedDescription>choose either ANL33, ANL70, ANL230,
              ANL1041, or ANL2082</DetailedDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="ANL33">ANL33</Value>
                <Value Enum="ANL70">ANL70</Value>
                <Value Enum="ANL230">ANL230</Value>
                <Value Enum="ANL1041">ANL1041</Value>
                <Value Enum="ANL2082">ANL2082</Value>
              </DiscreteInfo>
	    </String>

            <Double Name="egroupvals" Label="Energy Group Values (eV)"
                    NumberOfRequiredValues="1" Extensible="true">
              <BriefDescription>list the boundaries (not including 0)
              of the energy groups in eV</BriefDescription>
            </Double>

          </ChildrenDefinitions>

          <DiscreteInfo DefaultIndex="0">
	    <Structure>
              <Value Enum="Predefined">egroupname</Value>
	      <Items>
		<Item>egroupname</Item>
	      </Items>
	    </Structure>
	    <Structure>
              <Value Enum="Custom">egroupvals</Value>
	      <Items>
	        <Item>egroupvals</Item>
	      </Items>
	    </Structure>
          </DiscreteInfo>
        </String>

        <Int Name="scattering_order"
                Label="Scattering Order" NumberOfRequiredValues="1">
          <BriefDescription>list the boundaries (not including 0)
          of the energy groups in eV</BriefDescription>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Int>

	<String Name="inelastic_treatment"
                Label="Inelastic Scattering Treatment" Optional="true" IsEnabledByDefault="false">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Approximate">approximate</Value>
            <Value Enum="Rigorous">rigorous</Value>
          </DiscreteInfo>
	</String>

        <File Name="lumped_element_text_file" Label="Lumped Element Text File"
              Optional="true" IsEnabledByDefault="false" NumberOfValues="1" ShouldExist="true">
          <BriefDescription>Name of lumped element file</BriefDescription>
          <DetailedDescription>
            Name of file containing composition of the lumped element
            used in the material definition or in the decay chain.
          </DetailedDescription>
        </File>

        <Group Name="rzmflx_code_options" Label="RZ Core Options" AdvanceLevel="0">

          <BriefDescription>RZ Core options.</BriefDescription>

          <ItemDefinitions>

	    <String Name="code" Label="Code" AdvanceLevel="0" NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="twodant">twodant</Value>
              </DiscreteInfo>
	    </String>

	    <String Name="finegroup_egroupname" Label="Energy Group Structure"
                    AdvanceLevel="0" NumberOfRequiredValues="1">
              <BriefDescription>Predefined energy group</BriefDescription>
              <DetailedDescription>This is the fine-group structure
              that should be used in the RZ calculation. It is
              recommended to use the ANL1041 group structure. It
              should contain more energy groups than the one used with
              mcc3.</DetailedDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="ANL33">ANL33</Value>
                <Value Enum="ANL70">ANL70</Value>
                <Value Enum="ANL230">ANL230</Value>
                <Value Enum="ANL1041">ANL1041</Value>
                <Value Enum="ANL2082">ANL2082</Value>
              </DiscreteInfo>
	    </String>

            <String Name="R_boundaries"
                    Label="Boundaries in the R direction (m)"
                    NumberOfRequiredValues="1" Extensible="true"
                    Optional="true" IsEnabledByDefault="true">
              <BriefDescription>Boundaries of the different areas in the R direction.(Example: cylinder at radius 0.126125 should be written as cylinder0_126125)</BriefDescription>
            </String>

            <Double Name="R_nodes_distance"
                    Label="Distance between nodes in the R direction (m)"
                    NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="true">
              <BriefDescription>distance between each node in the R direction, recommended 0.05 m.</BriefDescription>
              <DefaultValue>.05</DefaultValue>
            </Double>

            <String Name="Z_boundaries"
                    Label="Boundaries in the Z direction (m)"
                    NumberOfRequiredValues="1" Extensible="true"
                    Optional="true" IsEnabledByDefault="true">
              <BriefDescription>Boundaries of the different areas in the Z direction.(Example: plane at z=38.0 should be written as z38_0)</BriefDescription>
            </String>

            <Double Name="Z_nodes_distance"
                    Label="Distance between nodes in the Z direction (m)"
                    NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="true">
              <BriefDescription>distance between each node in the Z direction, recommended 0.08 m.</BriefDescription>
              <DefaultValue>.08</DefaultValue>
            </Double>

            <Int Name="SN_angular_order"
                 Label="SN Angular Order" NumberOfRequiredValues="1">
              <BriefDescription>angular order for the transport approximation with SN method. Recommended ≥ 12</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
              <DefaultValue>18</DefaultValue>
            </Int>
            <String Name="core_2d_geometry"
                    Label="core 2d geometry"
                    NumberOfRequiredValues="1" Extensible="true"
                    Optional="true" IsEnabledByDefault="true" AdvanceLevel="11">
              <BriefDescription>A value needed by PyARC but is not documented. For now just hide it.</BriefDescription>
	      <DefaultValue>a</DefaultValue>
            </String>

          </ItemDefinitions>

        </Group>

      </ItemDefinitions>
    </AttDef>

  </Definitions>

</SMTK_AttributeSystem>
