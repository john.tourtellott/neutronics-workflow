<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Includes>
    <File>internal/templates/driver_input.sbt</File>
    <File>internal/templates/cross_section.sbt</File>
    <File>internal/templates/mesh.sbt</File>
    <File>internal/templates/material_assignment.sbt</File>
  </Includes>

  <Views>
    <View Type="Group" Name="Proteus-SN" Label="Proteus-SN" TopLevel="true"
      TabPosition="North" FilterByCategory="false" FilterByAdvanceLevel="false">
      <Views>
        <View Title="Driver input" />
        <View Title="Cross section" />
        <View Title="Mesh" />
        <View Title="Material assignment" />
      </Views>
    </View>

    <View Type="Instanced" Title="Driver input" Label="Driver input">
      <InstancedAttributes>
        <Att Name="driver_input-instance" Type="driver_input" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Cross section" Label="Cross section">
      <InstancedAttributes>
        <Att Name="cross_section-instance" Type="cross_section" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Mesh" Label="Mesh">
      <InstancedAttributes>
        <Att Name="mesh-instance" Type="mesh" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Material assignment" Label="Material assignment">
      <InstancedAttributes>
        <Att Name="material_assignment-instance" Type="material_assignment" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
